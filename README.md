# README #

Desafio Concluído por Daniel Mello Siqueira

### Conforme solicitado: ###

* Load dos shots com a imagem, título, e número de views
* Onclick carregando o shot, título, número de views, foto do autor e nome do autor
* Carregamento assíncrono das imagens e sob demanda
* Mapeamento JSON -> Objeto
* Cache de imagens
* Paginação On Scroll
* Utilização de Biblioteca AQuery

